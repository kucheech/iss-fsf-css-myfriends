/**
 * Server side code.
 */
"use strict";
console.log("Starting...");
var express = require("express");
var bodyParser = require("body-parser");

var app = express();
var fs = require('fs');
const NODE_PORT = process.env.PORT || 3000;

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(express.static(__dirname + "/../client/"));

//list of friends
var friends = {};
fs.readFile(__dirname + '/data.json', 'utf8',function(err, data) {
    if(err) throw err;
    friends = JSON.parse(data);
    //20170901 wku+ 
    for(var i in friends) {
        friends[i].id = uuidv4(); //add unique id
    }
});

//return the list of friends as an array
app.get("/friends", function (req, res) {
    res.json(friends);
});

//return the number of friends
app.get("/numfriends", function (req, res) {
    res.send("" + friends.length);
});

// Add friend record into  -- Charmaine
app.post("/addFriend", function (req, res) {
    // console.log("Received user object " + req.body);
    // console.log("Received user object " + JSON.stringify(req.body));
    // console.log("Friend Added!")
    //req.body.saved = 1; 
    //20170901 wku*: if input does not exists in list, then add it and send the list. 
    //Otherwise respond with a 422 error message
    var friend = req.body;
    if(isAlreadyInDb(friend) == false) {
        friend["id"] = uuidv4(); //assign an id
        friends.push(friend); //add new friend to list
        res.status(200).send(friends);
    } else {
        // res.status(422).json(friends); //alternative
        res.status(422).send("Friend already exists in database");    
    }    
    //console.log("friends[]>>>>>>>>");
    //console.log(friends);    
    //res.status(200);
});

// Delete friend   -- Siaw Chin
app.delete("/delfriend",function (req,res) {
    console.log("In app.delete >>>>>");
    
    var deleteId = req.query.id;
    var indexOfDelId;
    console.log("Del ID >>>" + deleteId);
    indexOfDelId = friends.findIndex(x => x.id==deleteId);
    console.log("indexOfDelId >>>>> " + indexOfDelId);
    friends.splice(indexOfDelId,1);
    console.log("friends[]>>>>>>>>");
    console.log(friends);
    res.status(200).json(friends);
});

// app.post("/addfriend", function (req, res) {
//     console.log("Received user object " + req.body);
//     console.log("Received user object " + JSON.stringify(req.body));

//     res.status(200);
// });

app.listen(NODE_PORT, function () {
    console.log("Web App started at " + NODE_PORT);
});

//make the app public. In this case, make it available for the testing platform
module.exports = app

//20170901 wku+: determine if input is already in db, based on tel number
function isAlreadyInDb(p) {
    for(var i in friends) {
        var f = friends[i];
        if(f.tel == p.tel) {
            return true;
        }
    }

    return false; //could not find match in array
}

//20170901 wku+: an id generator from https://stackoverflow.com/a/2117523
function uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }   

