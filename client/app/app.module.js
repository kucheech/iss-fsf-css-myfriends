(function () {
    "use strict";
    // angular.module("MyFriendsApp", []);
    // angular.module("MyFriendsApp", ['ds.clock']); //20170907 wku+ real-time clock from https://deepu.js.org/angular-clock/
    angular.module("MyFriendsApp", ['ds.clock', 'monospaced.qrcode']); //20170908 wku+ qrcode generator from https://github.com/monospaced/angular-qrcode
})();