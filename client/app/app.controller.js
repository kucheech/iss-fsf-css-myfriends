/**
 * Client side code.
 */
(function () {
    "use strict";
    angular.module("MyFriendsApp").controller("MyFriendsCtrl", MyFriendsCtrl);

    MyFriendsCtrl.$inject = ["$http", "$scope", "$filter", "MyFriendsService"]; //20170809 wku+ service

    function MyFriendsCtrl($http, $scope, $filter, MyFriendsService) {
        var self = this; // vm
        self.propertyName = "name"; //default sort parameter
        self.reverse = false;
        // self.numFriends = 0; //20170907 wku-: not using this
        self.friends = [];

        self.initForm = function () {

            //20170908 wku+ service
            MyFriendsService
                .getFriends()
                .then(function (result) {
                    console.log(result);
                    self.friends = result.data;
                }).catch(function (e) {
                    console.log(e);
                });

            //20170908 wku-
            // $http.get("/friends")
            //     .then(function (result) {
            //         console.log(result);
            //         self.friends = result.data;
            //         // self.numFriends = self.friends.length; //to use this instead of endpoint /numFriends
            //         //20170907 wku-: not using numFriends for the badge anymore
            //     }).catch(function (e) {
            //         console.log(e);
            //     });
        };

        self.initForm();

        self.sortBy = function (propertyName) {
            console.log("sortBy " + propertyName);
            self.reverse = (self.propertyName === propertyName) ? !self.reverse : false;

            console.log("sortBy " + propertyName + ", reverse: " + self.reverse);
            self.propertyName = propertyName;
        }

        self.delete = function (friend) {
            console.log("delete");
            var index = self.friends.indexOf(friend);
            if (index > -1) {
                self.friends.splice(index, 1);
            }
        }

    }

})();