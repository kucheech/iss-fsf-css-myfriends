## MyFriends App


### Project Members
* Clarence
* Charmaine
* Daniel
* Julien
* Mike
* Siaw Chin
* William

### Project setup
1. ```git clone https://kucheech@bitbucket.org/kucheech/iss-fsf-css-myfriends.git```
1. Install the dependencies using ```npm install``` and ```bower install```
1. Run using ```nodemon```
1. Open browser

### Update Repository Procedure
1. Update local repository with remote changes  
	`git pull`
1. Check git changes done on local repository and compare  
	`git status`
1. Add all changes to local INDEX  
	`git add *` All files and directories in current directory  
	`git add .` All files and directories in current directory plus those names starting with . (i.e. hidden)
1. Commit changes  
	`git commit -m "[<Name>] <Message>"`
1. Push changes to remote repository  
	`git push origin master`
1. Connect local repository to remote repository  
	`git remote add origin <server>` 
1. Switch to master branch  
	`git checkout master`
1. Create new branch  
	`git checkout -b <new_feature>`
1. Switch to master branch  
	`git checkout master`
1. Delete old branch  
	`git branch -d <old_feature>`
1. Push branch to remote repository  
	`git push origin <new_feature>`
1. Merge changes from master branch  
	`git merge master`
1. View changes between master and new_feature branch  
	`git diff master <new_feature>`

### Deployed to Heroku
View app at https://enigmatic-harbor-14853.herokuapp.com/